const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const dayjs = require('dayjs');

const BUCKET = process.env.BUCKET || 'linhaobin-index';
const SRC_DIR = process.env.SRC_DIR || 'public';
const LOG_DIR = process.env.LOG_DIR || 'upload_logs';

if (!BUCKET) throw new Error('BUCKET is empty');

// mkdir logs dir if not exists
if (!fs.existsSync(path.resolve(__dirname, LOG_DIR))) {
  fs.mkdirSync(path.resolve(__dirname, LOG_DIR));
}

const srcDir = path.resolve(__dirname, SRC_DIR);

const commonFilename = dayjs().format('YYYY-MM-DD-HH-mm-ss');
const logFile = path.resolve(__dirname, LOG_DIR, `${commonFilename}.log`);
const successList = path.resolve(__dirname, LOG_DIR, `${commonFilename}-success-list`);
const failureList = path.resolve(__dirname, LOG_DIR, `${commonFilename}-failure-list`);
const overwriteList = path.resolve(__dirname, LOG_DIR, `${commonFilename}-overwrite-list`);

const command = `qshell qupload2 --src-dir=${srcDir} --bucket=${BUCKET} --log-file=${logFile} --success-list=${successList} --failure-list=${failureList} --overwrite-list=${overwriteList} --overwrite --rescan-local`;
exec(command, (err, stdout, stderr) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(`stdout: ${stdout}`);
  console.log(`stderr: ${stderr}`);
});
